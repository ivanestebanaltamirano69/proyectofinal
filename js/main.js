
  // Import the functions you need from the SDKs you need
  import { initializeApp } from "https://www.gstatic.com/firebasejs/10.12.2/firebase-app.js";
  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries
  import { getDatabase ,onValue, ref as refS, set, child, get, update, remove } from
  "https://www.gstatic.com/firebasejs/10.12.2/firebase-database.js";
  //store
  import { getStorage, ref, uploadBytesResumable, getDownloadURL }
  from "https://www.gstatic.com/firebasejs/10.12.2/firebase-storage.js";
  // Your web app's Firebase configuration
  const firebaseConfig = {
    apiKey: "AIzaSyCmWHOw-ZqVkPbJfP4WJmiX7RkE9Lrglms",
    authDomain: "proyectowebfinalp.firebaseapp.com",
    databaseURL: "https://proyectowebfinalp-default-rtdb.firebaseio.com",
    projectId: "proyectowebfinalp",
    storageBucket: "proyectowebfinalp.appspot.com",
    messagingSenderId: "446510836893",
    appId: "1:446510836893:web:673848fef1c306c8e5f874"
  };
  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  const db = getDatabase(app);
  const storage = getStorage();

  var numSerie = 0;
  var marca = "";
  var modelo = "";
  var descripcion = "";
  var urlImag = "";

const imageInput = document.getElementById('imageInput');
const uploadButton = document.getElementById('uploadButton');
const progressDiv = document.getElementById('progress');
const txtUrlInput = document.getElementById('txtUrl');

function leerInputs(){
  numSerie = document.getElementById('txtNumSerie').value;
  marca = document.getElementById('txtMarca').value;
  modelo =  document.getElementById('txtModelo').value;
  descripcion = document.getElementById('txtDescripcion').value;
  urlImag = document.getElementById('txtUrl').value;
}

function mostarMensaje(mensaje){
  var mensajeElement = document.getElementById('mensaje');
  mensajeElement.textContent= mensaje;
  mensajeElement.style.display= 'block';
  setTimeout(()=>{
  mensajeElement.style.display ='none'},1000);
}

function limpiarInputs() {
  document.getElementById('txtNumSerie').value = '';
  document.getElementById('txtMarca').value = '';
  document.getElementById('txtModelo').value = '';
  document.getElementById('txtDescripcion').value = '';
  document.getElementById('txtUrl').value = '';
}

function escribirInputs() {
  document.getElementById('txtMarca').value = marca;
  document.getElementById('txtModelo').value = modelo;
  document.getElementById('txtDescripcion').value = descripcion;
  document.getElementById('txtUrl').value = urlImag;
}

function buscarProducto() {
  let numSerie = document.getElementById('txtNumSerie').value.trim();
  if(numSerie=== ""){
      mostarMensaje("No se ingreso Numero de Serie");
      return;
  }
  const dbref = refS(db);
    get(child(dbref, 'Tenis/' + numSerie)).then((snapshot) => {
        if(snapshot.exists()) {
            marca = snapshot.val().marca;
            modelo = snapshot.val().modelo;
            descripcion = snapshot.val().descripcion;
            urlImag = snapshot.val().urlImag;
            escribirInputs();
        } else {
            limpiarInputs();
            mostarMensaje("El Producto con Codigo" + numSerie + "No existe.");
        }
    });
}

btnBuscar.addEventListener('click', buscarProducto);
const btnAgregar = document.getElementById('btnAgregar');
btnAgregar.addEventListener('click', insertarProducto);
const btnActualizar = document.getElementById('btnActualizar');
btnActualizar.addEventListener('click', actualizarTenis);
const btnBorrar = document.getElementById('btnBorrar');
btnBorrar.addEventListener('click',eliminarTenis);

function insertarProducto(){
  alert("ingrese a add db");
  leerInputs();

  if(numSerie==="" || marca==="" || modelo==="" || descripcion===""){
      mostarMensaje("faltaron datos por capturar");
      return;
  }

  set(
       refS(db,'Tenis/' + numSerie),
       {
          numSerie:numSerie,
          marca:marca,
          modelo:modelo,
          descripcion:descripcion,
          urlImag:urlImag
       }
  ).then(()=>{

      alert("Se agrego con exito");
  }).catch((error)=>{
      alert("Ocurrio un error")
});
}

ListarProductos()
 function ListarProductos() {
    const dbRef = refS(db, 'Tenis');
    const tabla = document.getElementById('tablaProductos');
    const tbody = tabla.querySelector('tbody');
    tbody.innerHTML = '';
    onValue(dbRef, (snapshot) => {
        snapshot.forEach((childSnapshot) => {
         const childKey = childSnapshot.key;

         const data = childSnapshot.val();
         var fila = document.createElement('tr');

         var celdaCodigo = document.createElement('td');
         celdaCodigo.textContent = childKey;
         fila.appendChild(celdaCodigo);

         var celdaNombre = document.createElement('td');
         celdaNombre.textContent = data.marca;
         fila.appendChild(celdaNombre);

         var celdaPrecio = document.createElement('td');
         celdaPrecio.textContent = data.modelo;
         fila.appendChild(celdaPrecio);

         var celdaCantidad = document.createElement('td');
         celdaCantidad.textContent = data.descripcion;
         fila.appendChild(celdaCantidad);

         var celdaImagen = document.createElement('td');
         var imagen = document.createElement('img');
         imagen.src = data.urlImag;
         imagen.width = 100;
         celdaImagen.appendChild(imagen);
         fila.appendChild(celdaImagen);
         tbody.appendChild(fila);

    });
   },{onlyOnce: true});
 }
 function actualizarTenis () {
  leerInputs();
  if (numSerie ==="" || marca ==="" || modelo ==="" || descripcion === "" ){
      mostarMensaje("Favor de capturar toda la informacion.");
      return;
  }
  alert("actualizar");
  update(refS(db, 'Tenis/' + numSerie), {
      numSerie:numSerie,
      marca:marca,
      modelo:modelo,
      descripcion:descripcion,
      urlImag:urlImag
  }).then(() => {
     mostarMensaje("Se actualizo con exito.");
     limpiarInputs();
     ListarProductos();
  }).catch((error) =>{
      mostarMensaje("Ocurrio un error: " + error);
  });
}

function eliminarTenis (){
  let numSerie= document.getElementById('txtNumSerie').value.trim();
  if (numSerie === "") {
      mostarMensaje("No se ingreso un Codigo valido.");
      return;
  }

  const dbref = refS(db);
  get(child(dbref, 'Tenis/' + numSerie)). then((snapshot) => {

  if(snapshot.exists()){
      remove(refS(db, 'Tenis/' + numSerie))
      .then(() => {
          mostarMensaje("Producto eliminado con exito.");
          limpiarInputs();
          ListarProductos();
      })
      .catch((error) => {
          mostarMensaje("Ocurrio un error al eliminr el Producto:" + error);
      });
  } else {
      limpiarInputs();
      mostarMensaje("El Producto con ID" + numSerie + "no existe.");
  }
  });
  ListarProductos();
}

uploadButton.addEventListener('click', (event) => {
  event.preventDefault();
  const file = imageInput.files[0];

  if (file) {
      const storageRef = ref(storage, file.name);
      const uploadTask = uploadBytesResumable(storageRef, file);
      uploadTask.on('state_changed', (snapshot) => {
          const progress = (snapshot.bytesTrasnsferred / snapshot.totalBytes) * 100;
          progressDiv.textContent = 'Progreso: ' + progress.toFixed(2) + '%';
       }, (error) => {
          console.error(error);
       }, () => {
          getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
              txtUrlInput.value = downloadURL;
              setTimeout(() => {
                  progressDiv.textContent = '';
              }, 500);
              }).catch((error) => {
                  console.error(error);
              });
          });
       }
      });

  
